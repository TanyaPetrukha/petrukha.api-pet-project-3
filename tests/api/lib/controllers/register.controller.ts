import { ApiRequest } from "../request";

export class RegisterController {
    async register(newUser: {
        id: number;
        avatar: string;
        email: string;
        userName: string;
        password: string;
    }) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body(newUser)
            .send();
        return response;
    }
}
