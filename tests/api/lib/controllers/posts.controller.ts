import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async createPost(newPost: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url("api/Posts")
            .headers({ Authorization: `Bearer ${accessToken}` })
            .body(newPost)
            .send();
        return response;
    }

    async getPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("api/Posts")
            .send();
        return response;
    }

    async getPostsInvalidURL() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("api/InvalidURL")
            .send();
        return response;
    }

    async getPostsUnauthenticated() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("api/Posts")
            .send();
        return response;
    }
    async getPostById(postId: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts/${postId}`)  // Replace with the actual endpoint for getting a post by ID
            .send();
        return response;
    }
    async likePost(likeData: { entityId: number; isLike: boolean; userId: number }, accessToken: string) {
        try {
            const apiRequest = new ApiRequest();
            const response = await apiRequest
                .prefixUrl(baseUrl)
                .method("POST")
                .url("api/Posts/like")
                .headers({ Authorization: `Bearer ${accessToken}`, "Content-Type": "application/json" }) 
                .body(likeData)
                .send();
            return response;
        } catch (error) {
            // Handle any errors that might occur during the API call
            console.error("Error while liking the post:", error);
            throw error;
        }
    }
}
