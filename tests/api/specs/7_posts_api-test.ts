import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkResponseTime, checkStatusCode,  } from "../../helpers/functionsForChecking.helper";
import { validateAuthorStructure, validateCommentStructure, validatePostStructure } from "../../helpers/functionsForCheckingPosts.helper";

const posts = new PostsController();
const schemas = require('./data/schemas_testDataPosts.json'); 
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Get List of Posts", () => {
    let response;

    before(async () => {
        response = await posts.getPosts();
    });

    it("should return a list of posts and return 200 status code", () => {
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

        const firstPost = response.body[0];
        validatePostStructure(firstPost);
    });

    it("should match the schema for all posts", () => {
        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts);
    });

    it("should return 404 status code when an invalid URL is used", async () => {
        const response = await posts.getPostsInvalidURL();
        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);
    });

    it("should return 401 status code when the user is not authenticated", async () => {
        const response = await posts.getPostsUnauthenticated();
        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });
    
    it("should return 404 error when getting post details with invalid id", async () => {
        const invalidPostId = 999999;

        const response = await posts.getPostById(invalidPostId);

        checkStatusCode(response, 404);
        checkResponseTime(response, 1000);
    });

});
