import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();
const auth = new AuthController();

describe("Like a Post", () => {
    let accessToken: string;
    let postId: number;

    before(async () => {
        let response = await auth.login("email.qa.test@gmail.com", "password");
        checkStatusCode(response, 200);

        accessToken = response.body.token.accessToken.token;
    });

    it("should create a new post and return 200 status code", async () => {
        const newPost = {
            authorId: 0,
            previewImage: "post_preview_image",
            body: "This is the post content.",
        };

        let response = await posts.createPost(newPost, accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000); // Check response time here

        postId = response.body.id;
    });

    it("should like a post and return 200 status code", async () => {
        const likeData = {
            entityId: postId,
            isLike: true,
            userId: 0,
        };

        let response = await posts.likePost(likeData, accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000); // Pass the expected response time here

        expect(response.body).to.be.an("object").that.has.property("isLike").that.is.true;
    });

    it("should return 401 status code if the user is not authenticated", async () => {
        const likeData = {
            entityId: postId,
            isLike: true,
            userId: 0,
        };

        // Make sure to pass the response and expected response time to checkStatusCode and checkResponseTime
        let response = await posts.likePost(likeData, "");
        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });
});
