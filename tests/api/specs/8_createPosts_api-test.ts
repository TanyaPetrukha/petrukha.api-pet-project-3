import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

const schemas = require('./data/schemas_testDataPosts.json'); 
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Create Post for All Users", () => {
    let accessToken: string;
    let createdUserId: number;

    before(`Login and get the token`, async () => {
        try {
            let response = await auth.login("email.qa.test@gmail.com", "password");

            // Console log to inspect the response body structure
            console.log("Login Response Body:", response.body);

            if (!response.body || !response.body.token || !response.body.token.accessToken || !response.body.token.accessToken.token) {
                throw new Error("Response does not have the expected structure");
            }

            accessToken = response.body.token.accessToken.token;
            const newUser = {
                id: 7,
                avatar: "string",
                email: "alex.qa.test@gmail.com",
                userName: "AlexQaNew",
            };

            const userResponse = await users.updateUser(newUser, accessToken);
            expect(userResponse.statusCode, `Status Code should be 204`).to.be.equal(204);

            createdUserId = newUser.id;
        } catch (error) {
            console.error("Error during login:", error);
            throw error;
        }
    });

    it(`Should create a new post and return 200 status code`, async () => {
        const newPost = {
            authorId: createdUserId,
            previewImage: "post_preview_image",
            body: "This is the post content.",
        };

        const response = await posts.createPost(newPost, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts); 
    });

    it("should return 401 status code if the user is not authenticated", async () => {
        const newPost = {
            authorId: createdUserId,
            previewImage: "post_preview_image",
            body: "This is the post content.",
        };
        const response = await posts.createPost(newPost, "");

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

    afterEach(() => {
        console.log("it was a test");
    });
});
