import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { PostsController } from '../lib/controllers/posts.controller';
import { expect } from 'chai';

const posts = new PostsController();

describe('Use test data set for creating posts', () => {
    const validPostData = [
        {
            authorId: 1,
            previewImage: 'valid_preview_image_1',
            body: 'This is a valid post.',
        },
        {
            authorId: 2,
            previewImage: 'valid_preview_image_2',
            body: 'Another valid post.',
        },
    ];

    validPostData.forEach((postData, index) => {
        it(`should create a valid post (${index + 1}) and return 200 status code`, async () => {
            const response = await posts.createPost(postData, 'accessToken');

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);

            // Additional assertions for valid data
            const createdPost = response.body;
            expect(createdPost).to.be.an('object');
            // ... Perform other assertions based on the schema
        });
    });
});
