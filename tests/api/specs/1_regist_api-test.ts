import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";

const register = new RegisterController();

describe("User Registration", () => {
    it("should register a new user and return 201 status code", async () => {
        const newUser = {
            id: 7,
            avatar: "string",
            email: "alex.qa.test@gmail.com",
            userName: "AlexQaNew",
            password: "ATest2023!",
        };

        const response = await register.register(newUser);

        expect(response.statusCode).to.be.equal(201, "Status Code should be 201");
    });

    it("should return 400 status code if the email format is invalid", async () => {
        const newUser = {
            id: 10,
            avatar: "string",
            email: "invalid_email_format", 
            userName: "JohnDoe",
            password: "yourPassword123",
        };

        const response = await register.register(newUser);

        expect(response.statusCode).to.be.equal(400, "Status Code should be 400");
        expect(response.body).to.have.property("message").that.includes("email");
    });

    it("should return 400 status code if the email is already in use", async () => {
        const newUser = {
            id: 11,
            avatar: "string",
            email: "existing_user@gmail.com", // Use an email that is already in use
            userName: "JohnDoe",
            password: "yourPassword123",
        };

        const response = await register.register(newUser);

        expect(response.statusCode).to.be.equal(400, "Status Code should be 400");
        expect(response.body).to.have.property("message").that.includes("email");
    });
});
