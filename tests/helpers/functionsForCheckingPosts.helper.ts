import { expect } from "chai";

export function validateAuthorStructure(author) {
    expect(author).to.be.an("object");
    expect(author).to.have.property("id").that.is.a("number");
    expect(author).to.have.property("avatar").that.is.a("string");
    expect(author).to.have.property("email").that.is.a("string");
    expect(author).to.have.property("userName").that.is.a("string");
}

export function validateCommentStructure(comment) {
    expect(comment).to.be.an("object");
    expect(comment).to.have.property("id").that.is.a("number");
    expect(comment).to.have.property("createdAt").that.is.a("string");

    validateAuthorStructure(comment.author);

    expect(comment).to.have.property("body").that.is.a("string");

    const reactions = comment.reactions;
    expect(reactions).to.be.an("array").that.is.not.empty;

    const firstReaction = reactions[0];
    expect(firstReaction).to.have.property("isLike").that.is.a("boolean");

    validateAuthorStructure(firstReaction.user);
}

export function validatePostStructure(post) {
    expect(post).to.have.property("id").that.is.a("number");
    expect(post).to.have.property("createdAt").that.is.a("string");

    validateAuthorStructure(post.author);

    expect(post).to.have.property("previewImage").that.is.a("string");
    expect(post).to.have.property("body").that.is.a("string");

    const comments = post.comments;
    expect(comments).to.be.an("array").that.is.not.empty;

    const firstComment = comments[0];
    validateCommentStructure(firstComment);

    const postReactions = post.reactions;
    expect(postReactions).to.be.an("array").that.is.not.empty;

    const firstPostReaction = postReactions[0];
    expect(firstPostReaction).to.have.property("isLike").that.is.a("boolean");

    validateAuthorStructure(firstPostReaction.user);
}
